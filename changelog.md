# Changelog:
## Version 1.1: (06.10.2018)
###Pad controlls changed to use a bitmask for every pad. 
The bitmap consist of states (0 for not pressed and a 1 for pressed) for every direction or button of a pad. 
The following directions and buttons are supported:
    PB_DPAD_LEFT = 1,           -- {number}         <CONSTANT> bitmap value for dpad left
    PB_DPAD_RIGHT = 2,          -- {number}         <CONSTANT> bitmap value for dpad right
    PB_DPAD_UP = 4,             -- {number}         <CONSTANT> bitmap value for dpad up
    PB_DPAD_DOWN = 8,           -- {number}         <CONSTANT> bitmap value for dpad down
    PB_LEFTSTICK_LEFT = 16,     -- {number}         <CONSTANT> bitmap value for leftstick left
    PB_LEFTSTICK_RIGHT = 32,    -- {number}         <CONSTANT> bitmap value for leftstick right
    PB_LEFTSTICK_UP = 64,       -- {number}         <CONSTANT> bitmap value for leftstick up
    PB_LEFTSTICK_DOWN = 128,    -- {number}         <CONSTANT> bitmap value for leftstick down
    PB_RIGHTSTICK_LEFT = 256,   -- {number}         <CONSTANT> bitmap value for rightstick left
    PB_RIGHTSTICK_RIGHT = 512,  -- {number}         <CONSTANT> bitmap value for rightstick right
    PB_RIGHTSTICK_UP = 1024,    -- {number}         <CONSTANT> bitmap value for rightstick up
    PB_RIGHTSTICK_DOWN = 2048,  -- {number}         <CONSTANT> bitmap value for rightstick down
    PB_BUTTON_A = 4096,         -- {number}         <CONSTANT> bitmap value for button a
    PB_BUTTON_B = 8192,         -- {number}         <CONSTANT> bitmap value for button b
    PB_BUTTON_X = 16384,        -- {number}         <CONSTANT> bitmap value for button x
    PB_BUTTON_Y = 32768,        -- {number}         <CONSTANT> bitmap value for button y
    PB_BUTTON_L1 = 65536,       -- {number}         <CONSTANT> bitmap value for button l1
    PB_BUTTON_R1 = 131072,      -- {number}         <CONSTANT> bitmap value for button r1
    PB_BUTTON_L2 = 262144,      -- {number}         <CONSTANT> bitmap value for button l2
    PB_BUTTON_R2 = 534288,      -- {number}         <CONSTANT> bitmap value for button r2
    PB_BUTTON_L3 = 1068576,     -- {number}         <CONSTANT> bitmap value for button l3
    PB_BUTTON_R3 = 2137152,     -- {number}         <CONSTANT> bitmap value for button r3
    PB_BUTTON_START = 4274314,  -- {number}         <CONSTANT> bitmap value for button start
    PB_BUTTON_SELECT = 8558628, -- {number}         <CONSTANT> bitmap value for button select

###There are also 3 values to test if there is any movement:
- Test if there are any dpad movements: PB_DPAD
- Test if there are any left stick movements: PB_LEFT_STICK
- Test if there are any right stick movements: PB_RIGHT_STICK

###Since the current version of löve (11.1) uses Lua 5.x a library for bit operations is included. 
If löve will change to lua >= 5.3 this library will be removed

###Transformed the library files into a module
All library files are now consolidated into konformitaet.lua

###OOP for controllers and controls
Each controller and each control are now classes who inherit from KFController / KFControl

###KFML as language independent json file
The definition of the KFML file now follows the json format. This means, that KFML is language independant. 

###Config file
The KonFormitaet global variables are now initialized as a config file in json format called kfConfig.json

###Custom initialization
The cInit file handles the import and the initialization of custom code. 

###Custom variables as seperate file
The initialization of custom variables is now seperated into the file cVars.lua as part of the configuration

###Sprite class in the kernel
The kernel now has a base class for sprites. Using this class is optional.