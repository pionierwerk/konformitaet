------------------------------------------------------------------------------------
-- @author þorN
-- @license zlib
-- @copyright þorN  for pionierwerk.eu 2018
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
--- Class that represents the KonFormitaet kernel.
--
-- @classmod KFKernel
------------------------------------------------------------------------------------

--- member variables.
-- @table members
KFKernel = {
    vars = nil,                 -- (tab)            the game variables
    
    model = nil,                -- (tab)            the actual state model
    view = nil,                 -- (func)           the actual state view
    controller = nil,           -- (kfController)   the actual state controller
    states = nil,               -- (tab)            the list of states
    controllers = nil,          -- (tab)            the list of controllers
    actual_state_name="init",   -- (string)         the name of the actual state
    state = nil,                -- (kfState)        the actual state
    transitions = nil,          -- (tab)            the list of transitions
    
    view_stack = nil,           -- (tab)            the stack for views. Used in the update function to draw these views
    state_stack = nil,          -- (tab)            the stack for states. Used to return to the privious stack
    callback_stack = nil,       -- (tab)            the stack for callbacks. Used to save functions on generic states like dialogs
    
    pads = nil,                 -- (tab)            the love2d controled pads
    fullscreen_success = 0,     -- (number)         the love2d status if switching to fullscreen was successful
    playfield = nil,            -- (kfPlayfield)    the love2d canvas
    sprites = nil,              -- (KFSprite)       renderable sprites

    pad_bitmask = nil,           -- (tab)            the actual direction
    test_trigger_state = 0,     -- (number)         [WAIT|PRESSED|RELEASED] the states of the test if triggered cycle
    high_score_data_base = nil, -- (kfHighscoredatabase) -- the high score data base
    max_chars_in_name = 0,      -- (number)         the maximum length of the player name in the high scores

    actual_game_type = "",      -- (string)         the type of game based on the attributes from the game type
    hs_actual_game_type = "",   -- (string)         the tpye of game based on the attributes from the game type choosen in the high score viewer
    time_delay = 0,             -- (number)         the amount of cyces to wait until the time is changed
    time = 0,                   -- (number)         the game time
    screen_type = 1,            -- (number)         [1 = windowed | 2 = fullscreen ]

    version_main = 0,           -- (number)         major version number. 
    version_feature = 0,        -- (number)         feature version number. Increase this if you add new functions
    version_minor = 0,          -- (number)         minor version number. Increase this each time you do a bunch of bug fixes
    
    score_file = "",            -- (string)         the path to the high score data base

    TRIGGER_PHASE_WAIT=0,       -- (number)         <CONSTANT>  in a trigger phase this value represents the state waiting that the trigger is pressed
    TRIGGER_PHASE_PRESSED=1,    -- (number)         <CONSTANT>  in a trigger phase this value represents the state the trigger was pressed
    TRIGGER_PHASE_RELEASED=2,   -- (number)         <CONSTANT>  in a trigger phase this value represents the state the trigger was released

    PAD_MASTER = 1,             -- (number)         <CONSTANT>  the pad that controls the menu

    COLOR_SELECTED = nil,       -- (tab)            <CONSTANT>  color for a selected menu control
    COLOR_WHITE = nil,          -- (tab)            <CONSTANT>  white
    COLOR_BLACK = nil,          -- (tab)            <CONSTANT>  black
    COLOR_GREY = nil,           -- (tab)            <CONSTANT>  light grey. Used in menus to have white and grey lines
    MODAL_DIALOG_BLACK_TRANSPARENCY = nil, -- (tab) <CONSTANT>  black 10% transparency. Used in modal dialoges

    MENU_MIN = 1,               -- (number)         <CONSTANT>  the minimum element in menus
    MENU_STEP = 1,              -- (number)         <CONSTANT>  the amount of menus to move on a up or down event

    DECREASING_COUNTER_TO_ZERO_REACHED = 0, --(number) <CONSTANT> the indication that a counter reached zero
    NUMBER_OF_PLAYER_IN_A_SINGLE_PLAYER_GAME = 1, -- (number) <CONSTANT>  number of players in a single player game (1)
    MIN_NUMBER_OF_PLAYER_IN_A_MULTI_PLAYER_GAME = 2,  -- (number) <CONSTANT>  minimum number of players in a multi player game (2) 
    OBJECTS_NOT_EXIST = 0,      -- (number)         <CONSTANT>  the amount of objects is zero
    NEGATION = -1,              -- (number)         <CONSTANT>  used in terms like var * -1. Meaning is that the value turns from negative to positive or whiseverse
    CENTER = 2,                 -- (number)         <CONSTANT>  used in terms like WIDTH - Lenght / 2. Meaning is to centered a y position by deviding the space by 2
    SCROLLER_BORDER_8 = 101,    -- (number)         <CONSTANT>  number of chars to display
    ADD_ONE_EMPTY_LINE = 1,     -- (number)         <CONSTANT>  add 1 o the number of rows to have an empty line

    MODE_LIFES = 1,             -- (number)         <CONSTANT>  lifes mode
    MODE_TIME = 2,              -- (number)         <CONSTANT>  time mode
    lifes_or_time_mode = 0,     -- (number)         lifes (1) or time mode (2)
    hs_lifes_or_time_mode =0,   -- (number)         lifes (1) or time mode (2)

    TO_RELATION = "2",          -- (string)         <CONSTANT>  string between two states that represetns a relation e.g. "2"
       
    QUIT_STATE = "quit",        -- (string)         <CONSTANT>  the name of the quit state
    MENU_STATE = "menu",        -- (string)         <CONSTANT>  the name of the menu state

    VIEW_NAMESPACE = "kfcViews",-- (string)         <CONSTANT>  name of the views namespace
    PB_DPAD_LEFT = 1,           -- {number}         <CONSTANT> bitmask value for dpad left
    PB_DPAD_RIGHT = 2,          -- {number}         <CONSTANT> bitmask value for dpad right
    PB_DPAD_UP = 4,             -- {number}         <CONSTANT> bitmask value for dpad up
    PB_DPAD_DOWN = 8,           -- {number}         <CONSTANT> bitmask value for dpad down
    PB_LEFTSTICK_LEFT = 16,     -- {number}         <CONSTANT> bitmask value for leftstick left
    PB_LEFTSTICK_RIGHT = 32,    -- {number}         <CONSTANT> bitmask value for leftstick right
    PB_LEFTSTICK_UP = 64,       -- {number}         <CONSTANT> bitmask value for leftstick up
    PB_LEFTSTICK_DOWN = 128,    -- {number}         <CONSTANT> bitmask value for leftstick down
    PB_RIGHTSTICK_LEFT = 256,   -- {number}         <CONSTANT> bitmask value for rightstick left
    PB_RIGHTSTICK_RIGHT = 512,  -- {number}         <CONSTANT> bitmask value for rightstick right
    PB_RIGHTSTICK_UP = 1024,    -- {number}         <CONSTANT> bitmask value for rightstick up
    PB_RIGHTSTICK_DOWN = 2048,  -- {number}         <CONSTANT> bitmask value for rightstick down
    PB_BUTTON_A = 4096,         -- {number}         <CONSTANT> bitmask value for button a
    PB_BUTTON_B = 8192,         -- {number}         <CONSTANT> bitmask value for button b
    PB_BUTTON_X = 16384,        -- {number}         <CONSTANT> bitmask value for button x
    PB_BUTTON_Y = 32768,        -- {number}         <CONSTANT> bitmask value for button y
    PB_BUTTON_L1 = 65536,       -- {number}         <CONSTANT> bitmask value for button l1
    PB_BUTTON_R1 = 131072,      -- {number}         <CONSTANT> bitmask value for button r1
    PB_BUTTON_L2 = 262144,      -- {number}         <CONSTANT> bitmask value for button l2
    PB_BUTTON_R2 = 534288,      -- {number}         <CONSTANT> bitmask value for button r2
    PB_BUTTON_L3 = 1068576,     -- {number}         <CONSTANT> bitmask value for button l3
    PB_BUTTON_R3 = 2137152,     -- {number}         <CONSTANT> bitmask value for button r3
    PB_BUTTON_START = 4274314,  -- {number}         <CONSTANT> bitmask value for button start
    PB_BUTTON_SELECT = 8558628, -- {number}         <CONSTANT> bitmask value for button select
    PB_LEFTSTICK = 0,           -- {number}         <CONSTANT> bitmask value for all left stick directions
    PB_RIGHTSTICK = 0,          -- {number}         <CONSTANT> bitmask value for all right stick directions
    PB_DPAD = 0                 -- {number}         <CONSTANT> bitmask value for all dpad directions
}

KFKernel.__index = KFKernel

setmetatable(KFKernel, {
    __call = function (cls, ...)
        return cls.new(...)
    end
})

------------------------------------------------------------------------------------
--- Constructor.
-- @tab                     framework_vars          the global framework variables of the application
-- @tab                     custom_vars             the custom global variables of the application
function KFKernel:new(framework_vars,custom_vars)
    local self = setmetatable({}, KFKernel)

    -- system 
    -- -----------------------------------------------------------------------------
    math.randomseed( os.time() )                                                    -- initialize random seed

    -- member initialization
    -- -----------------------------------------------------------------------------
    self.states = {}
    self.controllers = {}
    self.transitions = {}
    self.view_stack = {}
    self.state_stack = {}
    self.callback_stack = {}
    self.pads = {}
    self.pad_bitmask = {0}
    self.sprites = {}

    -- constant initialization
    -- -----------------------------------------------------------------------------    
    self.COLOR_SELECTED = {1,0.6,0.2,1}
    self.COLOR_WHITE = {1,1,1,1}
    self.COLOR_BLACK = {0,0,0,1}
    self.COLOR_RED = {1,0,0,1}
    self.COLOR_GREY = {0.6,0.6,0.6,1}
    self.MODAL_DIALOG_BLACK_TRANSPARENCY = {0,0,0,0.9}
    self.PB_LEFTSTICK = self.PB_LEFTSTICK_UP + self.PB_LEFTSTICK_DOWN + self.PB_LEFTSTICK_LEFT + self.PB_LEFTSTICK_RIGHT
    self.PB_RIGHTSTICK = self.PB_RIGHTSTICK_UP + self.PB_RIGHTSTICK_DOWN + self.PB_RIGHTSTICK_LEFT + self.PB_RIGHTSTICK_RIGHT
    self.PB_DPAD = self.PB_DPAD_UP + self.PB_DPAD_DOWN + self.PB_DPAD_LEFT + self.PB_DPAD_RIGHT

    -- models
    -- -----------------------------------------------------------------------------
    self.vars = kfUtils:mergeTable(framework_vars, custom_vars)                     -- merge input tables to build vars table
    self.high_score_data_base = KFHighScoreDataBase:new(self.vars["kf_score_file"]) -- create new high score data base
    self.test_trigger_state = self.TRIGGER_PHASE_WAIT                               -- set initial value for a trigger wait
    self.time_delay = self.vars["kf_time_delay_duration"]                           -- set the duration to change the timer
    self.screen_type = self.vars["kf_default_screen_type"]                          -- set default screen type
    self.lifes_or_time_mode = self.MODE_LIFES                                         -- default = lifes mode
    self.hs_lifes_or_time_mode = self.MODE_LIFES                                      -- default = lifes (1) or time mode (2)

    -- high score data base
     if love.filesystem.getInfo(self.vars["kf_score_file"],"file") ~=nil then       -- check if he high score data base file exists
        local contents,size = love.filesystem.read(self.vars["kf_score_file"])      -- import high score data base from file
        self.high_score_data_base.tables = binser.deserialize(contents)[1]          -- deserialize data
    end

    -- love2D 
    -- -----------------------------------------------------------------------------
    self.pads = love.joystick.getJoysticks( )                                       -- get connected pads
    self:clearAllDirectionsAndButtons()                                             -- initialize controller dependend tables

    love.mouse.setVisible(false)                                                    -- set mouse cursor off
    self.playfield = KFPlayfield:new(self.vars["kf_width"],self.vars["kf_height"])  -- initialize screen
    love.window.setTitle(self.vars["kf_window_title"])                              -- set window title

    return self
end

-- love2d wrapper function
-- ================================================================================================================================

------------------------------------------------------------------------------------
-- This function is processed when the main loop processes the love2d update function.<br/>
-- - test if some keys where pressed and trigger thair functions<br/>
-- - calls the controller
function KFKernel:update()
    if love.keyboard.isDown("escape") then                                          -- test if the application should be finished
        os.exit()
        self:setState(self.QUIT_STATE)                                              -- quit programm
    end

    if love.keyboard.isDown("f8") then                                              -- test if the actual game should be finished 
        self:setState(self.MENU_STATE)                                              -- quit game round and return to the menu state
    end

    self.controller:main()                                                          -- calls the controller

    collectgarbage()                                                                -- do the carbage collection
end

------------------------------------------------------------------------------------
-- This function is processed when the main loop processes the love2d draw function.
function KFKernel:draw()
    love.graphics.scale(self.playfield.max_width / self.vars["kf_width"],self.playfield.max_height / self.vars["kf_height"])  -- scale the graphics to meet the physical screen resolution

    for key,draw_stacked_view in pairs(self.view_stack) do                          -- loop stacked views
        assert(loadstring('return ' .. self.VIEW_NAMESPACE ..':'..draw_stacked_view..'(...)'))()  -- draw stacked view
    end

    kfcViews[self.view]()                                                           -- draw the view of the actual state
end

-- Konformitaet Meta Language (KFML) functions
-- ================================================================================================================================

------------------------------------------------------------------------------------
--- process KFML file and create states.
-- @string                  meta_data               the KFML data
function KFKernel:processKFML(meta_data)
    for state_name,state_data in pairs(meta_data) do                                -- loop over states
        KFState:new(state_name,state_data)                                          -- create new states
    end 
end

-- FSE functions
-- ********************************************************************************************************************************

------------------------------------------------------------------------------------
--- change to the start state.</br>
-- If there is a transition init2[name start_state], this transition will be processed
function KFKernel:start()
    self:setState(kf.vars["kf_start_state"])                                        -- change to start state

    -- set Game Type
    -- -----------------------------------------------------------------------------
    kf:calculateGameType()                                                          -- calculate actual game type. This creates the table for the actual game type
end

------------------------------------------------------------------------------------
--- change a state<br/>
-- - call old state finally function<br/>
-- - call transition functon if there is one</br>
-- - change to new state</br>
-- - call new state init function
-- @string                  new_state_name          name of the state to change to
-- @tab                     additional_attributes   attributes to add to the model of the new state
function KFKernel:setState(new_state_name,additional_attributes) 
    local _old_state_name = self.actual_state_name                                  -- save the actual state name 

    -- old state finally function
    -- -----------------------------------------------------------------------------
    if self.controller ~= nil then
        self.controller:finally()                                                   -- call finally functon of old statte
    end

    -- transition
    -- -----------------------------------------------------------------------------
    local _transition_name = self.actual_state_name .. self.TO_RELATION .. new_state_name  -- calculate transition name

    if self.transitions[_transition_name] then                                      -- test if such a transition exists
        local _transition_function_name = self.transitions[_transition_name].transition_function_name -- get transition function nam
        kfcTransitions[_transition_function_name]()                                 -- call transition function
    end

    -- change state
    -- -----------------------------------------------------------------------------
    self.actual_state_name = new_state_name                                         -- set new state name
    self.state=self.states[new_state_name]                                          -- set new state
    
    -- handle model
    if self.state["model"] ~= nil then                                              -- test if the new state has a model
        self.model = self.state.model                                               -- set model

        if additional_attributes ~= nil then                                        -- test if there are additonal attribues for the model
            for key,value in pairs(additional_attributes) do                        -- loop of the additonal attributes
                self.model[key] = value                                             -- add additonal attributes into the model
            end

            self.callback_stack[#self.callback_stack+1] = self.model["callbacks"]   -- put callback on the stack
        end 

        -- handle stack
        if self.model["isStacked"] == true then                                     -- test if the new stae should be stacked
            self.view_stack[#self.view_stack+1] = self.view                         -- put view on the stack
            self.state_stack[#self.state_stack+1] = _old_state_name                 -- put state on the stack
        end
    end

    self:clearAllDirectionsAndButtons()                                             -- reset directions and button values
    self.test_trigger_state= TRIGGER_PHASE_WAIT                                     -- reset the test if triggered status

    -- handle view
    if self.state["view"] ~= nil then                                               -- test if the new state has a view
        self.view = self.state.view                                                 -- set view
    end

    -- handle controller
    if self.state["controller"] ~= nil then                                         -- test if the new state has a controller
        self.controller = self.state.controller                                     -- set controller
    end

    -- new state init function
    -- -----------------------------------------------------------------------------
    self.controller:init()                                                          -- call new state init function
end

------------------------------------------------------------------------------------
-- change to parent state that resides in the stack
function KFKernel:changeToParentStateFromTheStack()
    local _next_state = self.state_stack[#self.state_stack]                         -- get parent state from the stack

    self:removeLastStateFromTheStack()                                              -- remove the state from the stack
    self:setState(_next_state)                                                      -- change to the parent state 
end

------------------------------------------------------------------------------------
-- remove the last state from the stack
function KFKernel:removeLastStateFromTheStack()
    self.view_stack[#self.view_stack] = nil                                         -- clean view stack
    self.state_stack[#self.state_stack] = nil                                       -- clean state
    self.callback_stack[#self.callback_stack] = nil                                 -- clean callbacks
end

------------------------------------------------------------------------------------
--- Post game over. Test if a score enters the high scores.
function KFKernel:endGame()
    self.states["game_over"].model["next"] = "high_score_table"                     -- set the target state in the game over state to the high score table 

    for i,player in pairs(self.vars["kf_players"]) do                               -- loop over players
        -- test if the score is highter than the last score in the actual high score table      
        if player.score > self.high_score_data_base.tables[self.actual_game_type][#self.high_score_data_base.tables[self.actual_game_type]].points then
            if self.vars["kf_number_of_players"] == self.NUMBER_OF_PLAYER_IN_A_SINGLE_PLAYER_GAME then -- test if this is a single player game
                self.states["game_over"].model["next"] = "name"                     -- yes: set the target state in the game_over state to the state to enter the name
            else
                self.high_score_data_base:addScore("Player " .. i, player.score)    -- add the score into the high score table
            end
        end
    end

    kf:setState("game_over")                                                        -- change to the game_over state
end

-- Game model functions
-- ********************************************************************************************************************************

------------------------------------------------------------------------------------
--- calulates a unique key based on the values of the given attributes.
-- @string                  variable_to_set         the name of the variable to set ["actual_game_type|hs_actual_game_type]</br>
-- @string                  high_score_type_state   the state where the controls are defined that are part of the game type
function KFKernel:calculateGameTypeFunction(variable_to_set,high_score_type_state)
    local _key = kf.vars["kf_number_of_players"].."|"

    for key,control in pairs(self.states[high_score_type_state].controls) do        -- go over all controls
        if control.model["isPartOfGameType"] then                                   -- test if the control is part of game type
            if control.model["display_rule"] ~= nil then                            -- test if there is a display rule in the control
                if assert(loadstring('return ' .. control.model["display_rule"]..'(...)'))() then -- yes: test if display rule is positiv
                    _key = _key .. control.model["name"] .. self.vars[control.model["attribute"]] .. "|" -- yes: get the attribute name and its value and add them to the game type 
                end
            else
                _key = _key .. control.model["name"] .. self.vars[control.model["attribute"]] .. "|" -- -no, there is no display rule: get the attribute name and its value and add them to the game type 
            end
        end
    end

    _key:sub(1, -2)                                                                 -- delete the last |
    self[variable_to_set] = _key                                                    -- set specified game type
     
    self.high_score_data_base:addTableIfNotExists(_key)                             -- create high score table if it does not exists
end

------------------------------------------------------------------------------------
--- calulates a unique key based on the actual values of the attributes that control the type of game.
function KFKernel:calculateGameType()
    self:calculateGameTypeFunction("actual_game_type","menu")
end

------------------------------------------------------------------------------------
--- calulates a unique key based on the actual values of the attributes that control the type of game in the high score viewer.
function KFKernel:calculateHsGameType()
    self:calculateGameTypeFunction("hs_actual_game_type", "high_score_viewer")
end

------------------------------------------------------------------------------------
--- handle the delay and change the time. 
-- @number                  step                    the value that is added or subtrackted from the time
function KFKernel:handleTime(step)
    if self.time_delay > 0 then                                                     -- test if delay is over
        self.time_delay = self.time_delay - 1                                       -- no: decrease delay
    else
        self.time_delay = self.vars["kf_time_delay_duration"]                       -- yes: reset delay to start value
        self.time = self.time + step                                                -- change time
    end
end

-- Menu model functions
-- ================================================================================================================================

------------------------------------------------------------------------------------
--- go from fire status "wait" to fire status "released" via fire status "pressed"-
function KFKernel:testIfFireWasTriggeredAndReleased()
    if self.test_trigger_state == self.TRIGGER_PHASE_WAIT then                      -- test if the cycle state is WAIT
        if bit32.band(self.pad_bitmask[1], self.PB_BUTTON_A) == 0 then               -- test if there is no trigger
            self.test_trigger_state = self.TRIGGER_PHASE_PRESSED                    -- set the cycle state to PRESSED
        end 
    elseif self.test_trigger_state == self.TRIGGER_PHASE_PRESSED then               -- test if the cycle state is PRESSED 
        if bit32.band(self.pad_bitmask[self.PAD_MASTER], self.PB_BUTTON_A) > 0 then -- test if there is a trigger
            self.test_trigger_state = self.TRIGGER_PHASE_RELEASED                   -- set the cycle state to RELEASED
        end
    elseif self.test_trigger_state == self.TRIGGER_PHASE_RELEASED then              -- test if the cycle state is RELEASED
        if bit32.band(self.pad_bitmask[self.PAD_MASTER], self.PB_BUTTON_A) == 0 then-- test if there is no trigger
            self.test_trigger_state = self.TRIGGER_PHASE_WAIT                       -- reset trigger state to WAIT
            return true                                                             -- return cycle processed
        end
    end

    return false                                                                    -- return cycle not processed
end

------------------------------------------------------------------------------------
--- detects a dpad joystick movement or a direction key pressing. 
function KFKernel:setDirection()
    self.pad_bitmask = {0}
    self:clearAllDirectionsAndButtons()                                             -- reset directions and button values

    -- left stick
    if love.keyboard.isDown("w") then                                               -- test keyboard left stick up direction
        self.pad_bitmask[self.PAD_MASTER] = self.pad_bitmask[self.PAD_MASTER] + self.PB_LEFTSTICK_UP 
    end
    if love.keyboard.isDown("s") then                                               -- test keyboard left stick down direction
        self.pad_bitmask[self.PAD_MASTER] = self.pad_bitmask[self.PAD_MASTER] + self.PB_LEFTSTICK_DOWN
    end
    if love.keyboard.isDown("a") then                                               -- test keyboard left stick left direction
        self.pad_bitmask[self.PAD_MASTER] = self.pad_bitmask[self.PAD_MASTER] + self.PB_LEFTSTICK_LEFT
    end
    if love.keyboard.isDown("d") then                                               -- test keyboard left stick right direction
        self.pad_bitmask[self.PAD_MASTER] = self.pad_bitmask[self.PAD_MASTER] + self.PB_LEFTSTICK_RIGHT
    end

    -- right stick
    if love.keyboard.isDown("i") then                                               -- test keyboard right stick up direction
        self.pad_bitmask[self.PAD_MASTER] = self.pad_bitmask[self.PAD_MASTER] + self.PB_RIGHTSTICK_UP
    end
    if love.keyboard.isDown("k") then                                               -- test keyboard right stick down direction
        self.pad_bitmask[self.PAD_MASTER] = self.pad_bitmask[self.PAD_MASTER] + self.PB_RIGHTSTICK_DOWN
    end
    if love.keyboard.isDown("j") then                                               -- test keyboard right stick left direction
        self.pad_bitmask[self.PAD_MASTER] = self.pad_bitmask[self.PAD_MASTER] + self.PB_RIGHTSTICK_LEFT
    end
    if love.keyboard.isDown("l") then                                               -- test keyborad right stick right direction 
        self.pad_bitmask[self.PAD_MASTER] = self.pad_bitmask[self.PAD_MASTER] + self.PB_RIGHTSTICK_RIGHT
    end

    -- dpad
    if love.keyboard.isDown("up") then                                              -- test keypad up direction
        self.pad_bitmask[self.PAD_MASTER] = self.pad_bitmask[self.PAD_MASTER] + self.PB_DPAD_UP
    end
    if love.keyboard.isDown("down") then                                            -- test keypad down direction
        self.pad_bitmask[self.PAD_MASTER] = self.pad_bitmask[self.PAD_MASTER] + self.PB_DPAD_DOWN
    end
    if love.keyboard.isDown("left") then                                            -- test keypad left direction
        self.pad_bitmask[self.PAD_MASTER] = self.pad_bitmask[self.PAD_MASTER] + self.PB_DPAD_LEFT
    end
    if love.keyboard.isDown("right") then                                           -- test keypad right direction
        self.pad_bitmask[self.PAD_MASTER] = self.pad_bitmask[self.PAD_MASTER] + self.PB_DPAD_RIGHT
    end

    -- fire
    if love.keyboard.isDown("space") then                                           -- test keyboard fire
        self.pad_bitmask[self.PAD_MASTER] = self.pad_bitmask[self.PAD_MASTER] + self.PB_BUTTON_A
    end

    for pad_nr,pad in pairs(self.pads) do                                           -- loop over number of players
        -- left stick
        if pad:getGamepadAxis("leftx") == -1 then                                   -- test left stick left direction
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_LEFTSTICK_LEFT
        end

        if pad:getGamepadAxis("leftx") == 1 then                                    -- test left stick right direction
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_LEFTSTICK_RIGHT
        end

        if pad:getGamepadAxis("lefty") == -1 then                                   -- test left stick up direction
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_LEFTSTICK_UP
        end

        if pad:getGamepadAxis("lefty") == 1 then                                    -- test left stick down direction
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_LEFTSTICK_DOWN
        end

        -- right stick
        if pad:getGamepadAxis("rightx") == -1 then                                  -- test right stick left direction
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_RIGHTSTICK_LEFT
        end

        if pad:getGamepadAxis("rightx") == 1 then                                   -- test right stick right direction
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_RIGHTSTICK_RIGHT
        end

        if pad:getGamepadAxis("righty") == -1 then                                  -- test right stick up direction
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_RIGHTSTICK_UP
        end

        if pad:getGamepadAxis("righty") == 1 then                                   -- test right stick down direction
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_RIGHTSTICK_DOWN
        end

        if pad:getGamepadAxis('triggerleft') == 1 then                              -- test L2 trigger
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_BUTTON_L2
        end

        if pad:getGamepadAxis('triggerright') == 1 then                             -- test R2 trigger
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_BUTTON_R2
        end

        -- dpad
        if pad:isGamepadDown('dpup') then                                           -- test dpad up direction
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_DPAD_UP
        end
        if pad:isGamepadDown('dpdown') then                                         -- test dpad down direction
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_DPAD_DOWN
        end
        if pad:isGamepadDown('dpleft') then                                         -- test dpad left direction
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_DPAD_LEFT
        end
        if pad:isGamepadDown('dpright') then                                        -- test dpad rigth direction
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_DPAD_RIGHT
        end
        -- buttons
        if pad:isGamepadDown('a') then                                              -- test button a
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_BUTTON_A
        end
        if pad:isGamepadDown('b') then                                              -- test button b
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_BUTTON_B
        end
        if pad:isGamepadDown('x') then                                              -- test button x
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_BUTTON_X
        end
        if pad:isGamepadDown('y') then                                              -- test button y
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_BUTTON_Y
        end
        if pad:isGamepadDown('leftshoulder') then                                   -- test L1 trigger
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_BUTTON_L1
        end
        if pad:isGamepadDown('rightshoulder') then                                  -- test L2 trigger
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_BUTTON_R1
        end
        if pad:isGamepadDown('back') then                                           -- test back button
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_BUTTON_BACK
        end
        if pad:isGamepadDown('start') then                                          -- test start button
            self.pad_bitmask[pad_nr] = self.pad_bitmask[pad_nr] + self.PB_BUTTON_START
        end
    end
end

------------------------------------------------------------------------------------
--- call a callback function.
-- @tab                     callback                table<br/>
--            call: (string) the full qualified function name</br>
--           model: (tab) the model
function KFKernel:callCallbackFunction(callback)
    if callback ~= nil then                                                         -- test if there are callbacks
        if callback["model"] == nil then                                            -- test if the callback has a model
            assert(loadstring('return '..callback["call"]..'(...)'))()              -- no: call callback without a model
        else
            assert(loadstring('return '..callback["call"]..'(...)'))(callback["model"]) -- yes: call callback with a model
        end
    end
end

------------------------------------------------------------------------------------
--- init the timer with a start time and a delay value that defines the duration to change.
-- @number                  start_time              the time to start with
-- @number                  delay_duration          the duration to wait until the timer changes
function KFKernel:initTimer(start_time,delay_duration)
    self.time = start_time                                                          -- set time to the start value
    self.time_delay = duration_delay                                                -- set time delay to the start value
end

------------------------------------------------------------------------------------
--- clear all controller directions and button values.
function KFKernel:clearAllDirectionsAndButtons()
    self.pad_bitmask = {0}

    for key,pad in pairs(self.pads) do                                              -- initialize controller dependend tables of all connected controllers
        self.pad_bitmask[key] = 0                                                   -- clear directions and button status
    end
end

------------------------------------------------------------------------------------
--- add a sprite to the specified layer in the global sprites list.
-- @KFSprite                sprite                  the sprite to add
function KFKernel:addSprite(sprite)
    if self.sprites[sprite.layer] == nil then
        for c = 1,sprite.layer do
            if self.sprites[c] == nil then
                table.insert(self.sprites,c,{})
            end
        end
    end

    self.sprites[sprite.layer][sprite.unique_name] = sprite
end

------------------------------------------------------------------------------------
--- init the timer with a start time and a delay value that defines the duration to change.
-- @number                  start_time              the time to start with
function KFKernel:removeSprite(unique_name,layer_number)
    self.sprites[layer_number][unique_name] = nil
end

------------------------------------------------------------------------------------
--- Class that represents a FSE KFState.
--
-- @classmod KFState
------------------------------------------------------------------------------------

--- member variables
-- @table members
KFState = {
    model = nil,                -- (tab)           the model of the state
    view = nil,                 -- (func)          the view of the state. This view is rendered in the love2d draw function of the main loop
    controller = nil,           -- (func)          the code that is processed in the love2d update function of the main loop.
    controlls = nil             -- (tab)           the list of controls in the state. Controls are "sub" controller that encapsulate callback function of input events like fire & directions.
}

KFState.__index = KFState

setmetatable(KFState, {
    __call = function (cls, ...)
        return cls.new(...)
    end
})

------------------------------------------------------------------------------------
--- Constructor.
-- @string              name                        the name of the state<br/>
-- @tab                 configuration               the state configuration<br/>
--   model: (tab) the model of the state<br/>
--   view: (func) the view of the state. This view is rendered in the love2d draw function of the main loop<br/>
--   controller: (func) the code that is processed in the love2d update function of the main loop<br/>
--   controls: (tab) the controlls of the controller<br/>
--   transitions: (tab) model of transitions to other states<br/>
function KFState:new(name,state_data)
    local self = setmetatable({}, KFState)

    self.name = name
    self.model = state_data["model"]
    self.view = state_data["view"]

    if state_data["controller"] ~= nil then
        self.controller = assert(loadstring('return '..state_data["controller"]..':new'..'(...)'))(self.model) -- skip control if display_rule is negativ
    else
        self.controller = nil
    end
    
    self.controls = {}

    if state_data["controls"]["list"] ~= nil then                                   -- test if the state has controls
        for i,control in pairs(state_data["controls"]["list"]) do                   -- yes: loop over controls
            local _control = assert(loadstring('return '..control["type"]..':new'..'(...)'))(control["model"]) -- create a new control

            if state_data["controls"]["isOrdered"] then                             -- test if it's a ordered list (1,2,3...)
                table.insert(self.controls,_control)                                -- yes: Create new control based on the configuration and append it to controls
            else 
                self.controls[control["model"]["name"]] = _control                  -- no: it's a hashmap. Create new control based on the configuration and put it into controls
            end
        end
    end

    for key,transition in pairs (state_data["transitions"]) do                      -- loop over transitions
        local _transition_name = self.name .. kf.TO_RELATION .. key                 -- construct transition name
        kf.transitions[_transition_name] = KFTransition:new(_transition_name,transition) -- create transition 
    end

    kf.states[self.name] = self                                                     -- store state in states

    return self
end

------------------------------------------------------------------------------------
--- Class that represents a FSE KFTransition.
--
-- @classmod KFTransition
------------------------------------------------------------------------------------

--- member variables.
-- @table members
KFTransition = {
    transition_function_name = "" -- (string)       the transition function name. This function is processed during the transition
} 

KFTransition.__index = KFTransition

setmetatable(KFTransition, {
    __call = function (cls, ...)
        return cls.new(...)
    end
})

------------------------------------------------------------------------------------
--- Constructor.
-- @string                  name                    the name of the transition
-- @string                  transition_function_name the transition function name
function KFTransition:new(name,transition_function_name)
    local self = setmetatable({}, KFTransition)

    self.transition_function_name = transition_function_name                        -- set transition function name
    kf.transitions[name] = self                                                     -- add transition to transitions

    return self
end

------------------------------------------------------------------------------------
--- Class that represents a entry in a high score file.
--
-- @classmod KFSingleScore
------------------------------------------------------------------------------------

--- member variables.
-- @table members
KFSingleScore = {
    name = "",                  -- (string)         the name of the scorer
    points = 0                  -- (number)         the points of the score
} 

KFSingleScore.__index = KFSingleScore

setmetatable(KFSingleScore, {
    __call = function (cls, ...)
        return cls.new(...)
    end
})

------------------------------------------------------------------------------------
--- Constructor.
-- @string                  name                    the name of the scorer
-- @number                  points                  the points of the score
function KFSingleScore:new(name,points)
    local self = setmetatable({}, KFSingleScore)

    self.name = name
    self.points = points

    return self
end

------------------------------------------------------------------------------------
--- Class that represents the highscore file.
--
-- @classmod KFHighScoreDataBase
------------------------------------------------------------------------------------

--- member variables.
-- @table members
KFHighScoreDataBase = {
    player_name = "",           -- (string)         the name that was entered in the "enter your name" state
    path = "",                  -- (string)         the path to the database file
    tables = nil                -- (tab)            the game type high score tables
} 

KFHighScoreDataBase.__index = KFHighScoreDataBase

setmetatable(KFHighScoreDataBase, {
    __call = function (cls, ...)
        return cls.new(...)
    end
})

------------------------------------------------------------------------------------
--- constructor.
-- @string                  path                    path to the database file
function KFHighScoreDataBase:new(path)
    local self = setmetatable({}, KFHighScoreDataBase)

    self.player_name = ""
    self.path = path
    self.tables = {}

    return self
end

------------------------------------------------------------------------------------
--- if there is no table in the high score data base yet for
-- the given game type, a new one is created using the default names and scores.
-- @string                  game_type               the game type
function KFHighScoreDataBase:addTableIfNotExists(game_type)
    if self.tables[game_type] == nil then 
        self:FillTableWithDefaultValues(game_type)                                  -- creates a new table for the given game type
    end
end

------------------------------------------------------------------------------------
--- remove all high score tables from the database and save the empty file.
function KFHighScoreDataBase:resetAll()
    self.tables = {}                                                                -- clear tables
    kf:calculateGameType()                                                          -- calculate actual game type. This creates the table for the actual game type
    kf:calculateHsGameType()                                                        -- calculate actual high score viewer game type. This creates the table for the actual game type
    self:save()                                                                     -- save the data base file
end

------------------------------------------------------------------------------------
--- reset the actual table and save it. This fills the table.
-- with the default names and scores. 
function KFHighScoreDataBase:resetTable()
    self:FillTableWithDefaultValues(kf.hs_actual_game_type)                         -- reset actual high score viewer table
    self:save()                                                                     -- save the database file
end

------------------------------------------------------------------------------------
--- Fills a table with the default names and scores.
-- @string                  game_type               the game type of the table
function KFHighScoreDataBase:FillTableWithDefaultValues(game_type)
    local actual_scores = {}                                                        -- the new table

    -- fill the table with the default names and scores
    for i,score in pairs(kf.vars["kf_default_highscores"]) do                       -- loop over the default high score values
        actual_scores[i]=KFSingleScore:new(score[HIGHSCORE_NAME],score[HIGHSCORE_POINTS]) -- fill the table with the default values
    end

    self.tables[game_type]=actual_scores                                            -- put the table into the data base
end

------------------------------------------------------------------------------------
--- Fills a table with the default names and a negative (-1) score. 
-- This is used for example for Multi player high score tables
-- @string                  game_type               the game type of the table
function KFHighScoreDataBase:NegateScores(game_type)
    local actual_scores = {}                                                        -- the new table

    -- fill the table with the default names and a negativ score
    for i,score in pairs(kf.vars["kf_default_highscores"]) do                       -- loop over the default high score values
        actual_scores[i]=KFSingleScore:new(score[HIGHSCORE_NAME],kf.NEGATION)       -- fill the table with the default names and a negative score
    end

    self.tables[game_type]=actual_scores                                            -- put the table into the data base
end

------------------------------------------------------------------------------------
--- add a score to the high score table of the actual game type.
-- @string                  name                    the name of the scorer                  
-- @tab                     points                  the points 
function KFHighScoreDataBase:addScore(name,points)
    self.tables[kf.actual_game_type][#kf.vars["kf_default_highscores"]].name = name -- put the name of the actual player into the las entry of the actual high score table
    self.tables[kf.actual_game_type][#kf.vars["kf_default_highscores"]].points = points -- put the points into the last entry of the actual high score table
    
    table.sort(self.tables[kf.actual_game_type],function(a,b) return a.points > b.points end )  -- sort the table

    self:save()                                                                     -- save the data base
end

------------------------------------------------------------------------------------
--- save the data base into a file.
function KFHighScoreDataBase:save()
    love.filesystem.write(kf.vars["kf_score_file"],binser.serialize(self.tables))
end

------------------------------------------------------------------------------------
--- adds a char to the actual player name.
-- @tab                     model                   list of properties<br/>
--         char (string) the character to add
function KFHighScoreDataBase:addChar(model)
    if #self.player_name < kf.vars["kf_max_chars_in_name"] then                     -- test if the maximum of characters is already reached
        self.player_name = self.player_name .. model["char"]                        -- add the character at the end of the name 
    end
end

------------------------------------------------------------------------------------
--- remove the last character from the actual player name.
function KFHighScoreDataBase:deleteChar()
    if #self.player_name > 0 then                                                   -- test if the lenght of the player > 0
        self.player_name = self.player_name:sub(1,#self.player_name-1)              -- remove the last character from the actual player name
    end
end

------------------------------------------------------------------------------------
--- remove all characters from the actual player name.
function KFHighScoreDataBase:clearPlayerName()
    self.player_name = ""                                                           -- set the actual player name to an empty string
end

------------------------------------------------------------------------------------
--- Class that represents the screen the graphics are drawn to.
--
-- @classmod KFPlayfield
------------------------------------------------------------------------------------

--- member variables.
-- @table members   
KFPlayfield = {
    width= 0,                   -- (number)         the logical width of the screen.
    height= 0,                  -- (number)         the logical height of the screen. 
    max_width = 0,              -- (number)         the physical width of the screen
    max_height = 0              -- (number)         the physical height of the screen
} 
KFPlayfield.__index = KFPlayfield

setmetatable(KFPlayfield, {
    __call = function (cls, ...)
        return cls.new(...)
    end
})

------------------------------------------------------------------------------------
--- Constructor.
-- @number                     width                   the width of the screen
-- @number                     height                  the height of the screen
function KFPlayfield:new(width,height)
    local self = setmetatable({}, KFPlayfield)

    self.width = width
    self.height = height

    self:setFullscreen(width,height)                                                -- switch to fullscreen
    return self
end

------------------------------------------------------------------------------------
--- switch to Fullscreen mode.
-- @number                     width                   the width of the screen
-- @number                     height                  the height of the screen
function KFPlayfield:setFullscreen(width,height)
    love.window.setMode(width, height, {fullscreen=true, resizable=false, vsync=true, minwidth=width, minheight=height})

    -- get the physical resolution and the flags
    if kf == nil then                                                               -- workaround the lua oop bug
        self.max_width,self.max_height,self.flags = love.window.getMode()
    else
        self.max_width,self.max_height,self.flags = love.window.getMode()
    end
end

------------------------------------------------------------------------------------
--- switch to windowed mode.
-- @number                     width                   the physical width of the window
-- @number                     height                  the physical height of the window
function KFPlayfield:setWindowed(width,height)
    love.window.setMode(width,height, {fullscreen=false, resizable=false, vsync=true, minwidth=kf.vars["kf_width"], minheight=kf.vars["kf_height"]})

    -- get the physical resolution and the flags
    self.max_width,self.max_height,self.flags = love.window.getMode()
end

------------------------------------------------------------------------------------
--- change the actual screenmode. Switch from fullscreen 
-- to windowed and back.
function KFPlayfield:toggleScreenMode()
    local _fullscreen = nil
    local _fstype = nil

    _fullscreen, _fstype = love.window.getFullscreen()                              -- test if the actual mode is fullscreen
    if _fullscreen then
        self:setWindowed(kf.vars["kf_width"],kf.vars["kf_height"])                  -- yes: switch to windows mode
    else
        self:setFullscreen(kf.vars["kf_width"],kf.vars["kf_height"])                -- no: switch to fullscreen mode
    end
end

------------------------------------------------------------------------------------
-- Sprite class
--
-- @classmod KFSprite
------------------------------------------------------------------------------------

--- member variables.
-- @table members
-- @number                  x                       the x position of a player
-- @number                  y                       the y position of a player
-- @sheet                   sheet                   the actual image sheet
-- @image                   image                   the actual image
-- @tab                     collision_targets       a table of tables that represent the values to build the rectangle that is check during the collision detection
-- @tab                     moving_vector           the amount of pixels to move in x and y direction
-- @tab                     borders                 a rectangle that acts as border for the player
-- @string                  name                    the name of the sprite in an actors sprite list
-- @string                  unique_name             the name of the sprite in the global sprite list
-- @number                  layer                   the layer in the global sprites list
KFSprite = {
    x = 0, 
    y = 0,
    sheet = nil,
    quads = {},
    image = nil,
    collision_targets = {
        { x = 11, y = 4, width = 15, height = 23 }
    },
    moving_vector = {},
    borders = {},
    isActive= true,
    name = "",
    unique_name = "",
    layer = 0
} 
KFSprite.__index = KFSprite

------------------------------------------------------------------------------------
--- constructor.
-- @tab                     model                   the controller model
function KFSprite:new(name,unique_name,sheet,layer,isActive)
    local self = setmetatable({
        name = name,
        unique_name = unique_name,
        sheet = sheet,
        layer = layer,
        isActive = isActive,
        quads = {}
    }, KFSprite )

    return self
end

function KFSprite:setCoordinates(x,y)
    self.x = x
    self.y = y
end

function KFSprite:setSheet(sheet)
    self.sheet = sheet
end

function KFSprite:setQuads(amount,sx,sy,width,height)
    for i = 0,amount-1 do
        self.quads[i+1] = love.graphics.newQuad(sx + (width * i), sy, width, height, self.sheet:getDimensions())
    end
end

function KFSprite:setImage(quad_number)
    self.image = self.quads[quad_number]
end

function KFSprite:setCollionsTargets(collision_targets)
    self.collision_targets = collision_targets
end

function KFSprite:setBorders(borders)
    self.borders = borders
end

function KFSprite:setMovingVector(mx,my)
    self.moving_vector = {x=mx,y=my}
end

------------------------------------------------------------------------------------
-- Actor class
--
-- @classmod KFActor
------------------------------------------------------------------------------------

--- member variables.
-- @table members
-- @tab                     sprites                 the sprites that are connected to the actor
KFActor = {
    sprites = nil,
    isActive = false
} 
KFActor.__index = KFActor

------------------------------------------------------------------------------------
--- constructor.
-- @tab                     model                   the controller model
function KFActor:new(isActive)
    local self = setmetatable({
        sprites = {},
        isActive = isActive
    }, KFActor )

    return self
end

function KFActor:addSprite(sprite)
    self.sprites[sprite.name] = sprite
end

function KFActor:removeSprite(sprite_name)
    local _layer = self.sprites[sprite_name].layer
end

------------------------------------------------------------------------------------
--- Class that represents a controller in a state.
--
-- @classmod KFController
------------------------------------------------------------------------------------

--- member variables.
-- @table members
KFController = {
    model = nil                     -- (tab)            the controller model
}

KFController.__index = KFController

------------------------------------------------------------------------------------
-- constructor.
-- @tab                     model                   the controller model
function KFController:new(model)
    local self = setmetatable({
        model = model
    }, KFController )
 
    return self
end

function KFController:main()
end

function KFController:init()
end

function KFController:finally() 
end

-----------------------------------------------------------------------------------------------------------------------------------
--- Controller implementations.
--
-- @classmod kfConrollers
-----------------------------------------------------------------------------------------------------------------------------------

-- Controllers
-- ================================================================================================================================
-- Wait for timer controller
-- ********************************************************************************************************************************

--- member variables.
-- @table members
KFWaitForTimer = setmetatable({},{__index = KFController})
KFWaitForTimer.__index = KFWaitForTimer

-------------------------------------------------------------------------------------
--- constructor.
function KFWaitForTimer:new(model)
    local self = setmetatable(KFController.new(model), KFWaitForTimer)

    self.model = model

    return self
end

-------------------------------------------------------------------------------------
--- waits that a timer counts down to zero.</br>
--</br>
-- <u>Model</u></br>
-- next:                (string)                    state to change to after the timer is at zero</br>
-- delay:               (number)                    the time to wait
function KFWaitForTimer:main()
    self.model.timer = self.model.timer - 1                                             -- decrease timer 

    if self.model.timer == 0 then
        kf:setState(self.model.next)                                                  -- change state when timer is at zero
    end
end

------------------------------------------------------------------------------------
---  wait for timer init function.
function KFWaitForTimer:init()
    self.model.timer = self.model.delay                                                 -- set timer to start value from the model
end

------------------------------------------------------------------------------------
--- wait for time finally function.
function KFWaitForTimer:finally()
    self.model.timer = nil                                                            -- clear the timer interna variable
end

------------------------------------------------------------------------------------
--- Class that represents a controller in a state.
--
-- @classmod KFWaitForTimer
------------------------------------------------------------------------------------

--- member variables.
-- @table members
KFMenu = setmetatable({},{__index = KFController})
KFMenu.__index = KFMenu

-------------------------------------------------------------------------------------
--- constructor.
function KFMenu:new(model)
    local self = setmetatable(KFController.new(model), KFMenu)

    self.model = model

    return self
end

------------------------------------------------------------------------------------
-- up and down controller moving from one control to the next or previous in the list</br>
--</br>
-- <u>Model</u></br>
-- actual_control_number: the number of the actual control</br>
-- start_control_number:  the number of the control to start with after the state changed
function KFMenu:main() 
    if kf.pad_bitmask[kf.PAD_MASTER] == 0 then
        kf:setDirection()                                                           -- get direction
        
        local _control = kf.state.controls[self.model.actual_control_number]        -- get actual control of state 
        _control:main()                                                             -- call control controller

        if bit32.band(kf.pad_bitmask[kf.PAD_MASTER], kf.PB_DPAD_UP) > 0 then        -- test if direction "up" was triggered
            local _step = kf.MENU_STEP                                              -- initialize local step with the default value
            local _next_conrol_found = false                                        -- set found flag to false
            
            while ( not _next_control_found ) and ( self.model.actual_control_number - _step >= kf.MENU_MIN ) do -- loop while no control is visible and we are above the first controls 
                local _next_control = kf.state.controls[self.model.actual_control_number - _step] -- get next control  
  
                if _next_control.model["display_rule"] ~= nil then                  -- test if diplay rule exists
                    if assert(loadstring('return ' .. _next_control.model["display_rule"]..'(...)'))() then -- yes: test if display_rule is positiv
                        _next_conrol_found = true                                   -- set found flag to true
                        break                                                       -- end while loop
                    else
                        _step = _step + kf.MENU_STEP                                -- if display rule is negativ, decrease the local step and continue the while loop
                    end
                else
                    _next_conrol_found = true                                       -- if there is no display rule, set found flag to true
                    break                                                           -- end while loop
                end
            end

            if _next_conrol_found then                                              -- test if a displayable control was found
                self.model.actual_control_number = self.model.actual_control_number - _step  -- yes: decrease value
            end
        elseif bit32.band(kf.pad_bitmask[kf.PAD_MASTER], kf.PB_DPAD_DOWN) > 0 then  -- test if direction "down" was triggered
            local _step = kf.MENU_STEP                                              -- initialize local step with the default value
            local _next_conrol_found = false                                        -- set found flag to false
            
            while ( not _next_control_found ) and ( self.model.actual_control_number + _step <= #kf.states[kf.actual_state_name].controls ) do -- loop while no control is visible and the overall number of controls is not reached
                local _next_control = kf.state.controls[self.model.actual_control_number + _step] -- get next control  
  
                if _next_control.model["display_rule"] ~= nil then                  -- test if diplay rule exists
                    if assert(loadstring('return ' .. _next_control.model["display_rule"]..'(...)'))() then -- yes: test if display_rule is positiv
                        _next_conrol_found = true                                   -- set found flag to true
                        break                                                       -- end while loop
                    else
                        _step = _step + kf.MENU_STEP                                -- if display rule is negativ, increase the local step and continue the while loop
                    end
                else
                    _next_conrol_found = true                                       -- if there is no display rule, set found flag to true
                    break                                                           -- end while loop
                end
            end

            if _next_conrol_found then                                              -- test if a displayable control was found
                self.model.actual_control_number = self.model.actual_control_number + _step  -- yes: increase value
            end
        end
    else
        kf:setDirection()                                                           -- get direction
    end
end

------------------------------------------------------------------------------------
--- menu init function.
function KFMenu:init()
    kf:clearAllDirectionsAndButtons()                                               -- clear controller bitmasks

    if self.model.start_control_number ~= nil then                                  -- test if model attribute start_control_number is set
        self.model.actual_control_number = self.model.start_control_number          -- set actual_control_number to the one defined in start_control_number
    end
end

-- Form controller
-- ********************************************************************************************************************************
--- member variables.
-- @table members
KFForm = setmetatable({},{__index = KFController})
KFForm.__index = KFForm

-------------------------------------------------------------------------------------
--- constructor.
function KFForm:new(model)
    local self = setmetatable(KFController:new(model), KFForm)
    return self
end

------------------------------------------------------------------------------------
-- 4 way controls moving from one control to the next<br/> 
-- <br/> 
-- <u>Model:</u><br/> 
-- actual_control_name: the number of the actual control<br/> 
-- start_control_name:  the number of the control to start with after the state changed
function KFForm:main()
    if kf.pad_bitmask[kf.PAD_MASTER] == 0 then
        kf:setDirection()                                                           -- get direction
    
        -- call control controller
        local _control = kf.state.controls[kf.model.actual_control_name]            -- get actual control of state by name
        _control:main()                                                             -- call control controller
        -- left
        if bit32.band(kf.pad_bitmask[kf.PAD_MASTER], kf.PB_DPAD_LEFT) > 0 then      -- test if direction "left" was triggered
            self.model.actual_control_name = _control.model["next"]["left"]         -- set active control to the one specified in "left"
        -- right 
        elseif bit32.band(kf.pad_bitmask[kf.PAD_MASTER], kf.PB_DPAD_RIGHT) > 0 then -- test if direction "right" was triggered
            self.model.actual_control_name = _control.model["next"]["right"]        -- set active control to the one specified in "right"
        end
        -- up
        if bit32.band(kf.pad_bitmask[kf.PAD_MASTER], kf.PB_DPAD_UP) > 0 then        -- test if direction "up" was triggered
            self.model.actual_control_name = _control.model["next"]["up"]           -- set active control to the one specified in "up"
        -- down 
        elseif bit32.band(kf.pad_bitmask[kf.PAD_MASTER], kf.PB_DPAD_DOWN) > 0 then  -- test if direction "down" was triggered
            kf.model.actual_control_name = _control.model["next"]["down"]           -- set active control to the one specified in "down"
        end
    else
        kf:setDirection()                                                           -- get direction
    end
end

------------------------------------------------------------------------------------
--- form controller init function.
function KFForm:init()
    kf.pad_bitmask= {0}                                                             -- reset direction to no direction

    if self.model.start_control_name ~= nil then
        self.model.actual_control_name = self.model.start_control_name              -- set actual_control_name to the one defined in start_control_name
    end
end

-- Quit controller
-- ********************************************************************************************************************************
--- member variables.
-- @table members
KFQuit = setmetatable({},{__index = KFController})
KFQuit.__index = KFQuit

-------------------------------------------------------------------------------------
--- constructor.
function KFQuit:new(model)
    local self = setmetatable(KFController:new(model), KFQuit)
    return self
end

------------------------------------------------------------------------------------
--- quit the programm.
function KFQuit:main()
    kf.playfield:setWindowed(kf.vars["kf_width"], kf.vars["kf_height"])             -- set screen mode back to windowed
    os.exit()                                                                       -- quit program
end

------------------------------------------------------------------------------------
--- Class that represents a entry in a list of controls.
--
-- A conroll is used to encapsulate evens that change the model
-- For example is a menu entry a control. Or a character
-- in a  entry form.
--
-- @classmod KFControl
------------------------------------------------------------------------------------

--- member variables.
-- @table members
KFControl = {
    model = nil                 -- (tab)            the model of the control
}

KFControl.__index = KFControl

------------------------------------------------------------------------------------
--- constructor
-- @tab                     model                   the model of the control<br/>
--        type          the full qualified controller function<br/> 
--        attributes    the model
function KFControl:new(model)
    local self = setmetatable({
        model = model
    }, KFControl )
 
    if model["overloaded_functions"] ~= nil then
        for i,overload in pairs(model["overloaded_functions"]) do
            assert(loadstring('return '..overload..'(...)'))(model)                 -- skip control if display_rule is negativ
        end
    end

    self.model.displayAttributes = false

    return self
end

------------------------------------------------------------------------------------------------
--- increase / decrease a value.
-- @tab                 model                       the model<br/>
-- name:                (string)                    the name of the control<br/>
-- display_name:        (string)                    the display name of the control<br/>
-- callbacks:           (tab)                       table with callbacks <br/>
-- zero_value:          (number)                    specify a special value<br/>
-- zero_text:           (string)                    give the spcial vale a label<br/>
-- isPartOfGameType:    (bool)                      [true|false] if this flag is set, the controll is used to calculate the game type<br/>
-- min                  (number)                    minimum value (corresponds to first option)<br/>
-- max                  (number)                    maximum value (should correspond to the last option)<br/>
-- step                 (step)                      amount to increase or decrease the value<br/>
--<br/>
-- <u>type: text</u><br/>
-- attribute:           (string)                    the name of the global parameter to control<br/>
-- options_display_names: (tab)                     table with text optons<br/>
-- isSystemVariable     (bool)                      false (optional) variable is referenced from the global variables<br/>
--<br/>
-- <u>type: number</u><br/>
-- attribute:           (string)                    the name of the global parameter to control<br/>
-- isSystemVariable     (bool)                      false (optional) variable is referenced from the global variables<br/>
--<br/>
-- <u>system</u><br/> 
-- attribute            (string)                    the name of a kernel parameter
-- isSystemVariable:    (bool)                      true (mantory) variable is referenced from the kernel<br/>

--- member variables.
-- @table members
KFSelectNumber = setmetatable({},{__index = KFControl})
KFSelectNumber.__index = KFSelectNumber

-------------------------------------------------------------------------------------
--- constructor.
function KFSelectNumber:new(model)
    local self = setmetatable(KFControl:new(model), KFSelectNumber)
    self.model.displayAttributes = true
    return self
end

function KFSelectNumber:main()
    -- left
    if bit32.band(kf.pad_bitmask[kf.PAD_MASTER], kf.PB_DPAD_LEFT) > 0 then              -- test if direction is set to "left"
        if self.model["isSystemVariable"] ~= null and self.model["isSystemVariable"] then -- test if the attribute to change is a system attribute
            if kf[self.model["attribute"]] > self.model["min"] then                 -- yes: test if the attribute bound to the control is greater than the minimum
                kf[self.model["attribute"]] = kf[self.model["attribute"]] - self.model["step"]  -- decrease value
                kf:callCallbackFunction(self.model["callbacks"]["left"])            -- call callback function of the model
            end    
        else
            if kf.vars[self.model["attribute"]] > self.model["min"] then            -- no: test if the attribute bound to the control is greater than the minimum
                kf.vars[self.model["attribute"]] = kf.vars[self.model["attribute"]] - self.model["step"] -- decrease value
                kf:callCallbackFunction(self.model["callbacks"]["left"])            -- call callback function of the model
            end
        end
    -- right
    elseif bit32.band(kf.pad_bitmask[kf.PAD_MASTER], kf.PB_DPAD_RIGHT) > 0 then     -- test if direction is set to "right"
        if self.model["isSystemVariable"] ~= null and self.model["isSystemVariable"] then -- test if the attribute to change is a system attribute
            if kf[self.model["attribute"]] < self.model["max"] then                 -- yes: test if the attribute bound to the control is lower than the maximum
                kf[self.model["attribute"]] = kf[self.model["attribute"]] + self.model["step"] -- increase value
                kf:callCallbackFunction(self.model["callbacks"]["right"])           -- call callback function of the model
            end
        else
            if kf.vars[self.model["attribute"]] < self.model["max"] then            -- no: test if the attribute bound to the control is lower than the maximum
                kf.vars[self.model["attribute"]] = kf.vars[self.model["attribute"]] + self.model["step"] -- increase value
                kf:callCallbackFunction(self.model["callbacks"]["right"])           -- call callback function of the model 
            end
        end
    end
end

------------------------------------------------------------------------------------------------
--- changes the state.
-- @tab                 model                       the model<br/>
-- name:                (string)                    the name of the control<br/>
-- display_name:        (string)                    the display name of the control<br/>
-- state:               (string)                    the name of the state to go to<br/>
-- <br/>
-- <u>for dialogs</u><br/>
-- state_model:         (tab)                       table of properties<br/>
-- texts:               (tab)                       table pf texts to display in a dialog<br/>
-- x:                   (number)                    x position of dialog<br/>
-- y:                   (number)                    y position of dialog<br/>
-- width:               (number)                    width of the dialog<br/>
-- height:              (number)                    height of the dialog<br/>
-- isStacked:           (bool)                      flag to indicate that this state should be stacked<br/>
-- callbacks:           (tab)                       table of properties<br/>
-- fire:                (string)                    full qalified function name 

--- member variables.
-- @table members
KFChangeState = setmetatable({},{__index = KFControl})
KFChangeState.__index = KFChangeState

-------------------------------------------------------------------------------------
--- constructor.
function KFChangeState:new(model)
    local self = setmetatable(KFControl:new(model), KFChangeState)
    return self
end

function KFChangeState:main()
    if kf:testIfFireWasTriggeredAndReleased() then                                  -- test if fire was triggered and released                                                     
        if self.model["state_model"] == nil then                                    -- test if the model has a state_model attribute
            kf:setState(self.model["state"])                                        -- No: change the state without a state model 
        else
            kf:setState(self.model["state"],self.model["state_model"])              -- yes: change the state with a state model
        end
    end
end

------------------------------------------------------------------------------------------------
--- call the ok function in a dialog, then return to the parent dialog the function and the parent dialog are on the stack.
-- @tab                 model                       the model<br/>
-- name:                (string)                    the name of the control<br/>
-- display_name:        (string)                    the display name of the control

--- member variables.
-- @table members
KFCallOkFunction = setmetatable({},{__index = KFControl})
KFCallOkFunction.__index = KFCallOkFunction

-------------------------------------------------------------------------------------
--- constructor.
function KFCallOkFunction:new(model)
    local self = setmetatable(KFControl:new(model), KFCallOkFunction)
    return self
end

function KFCallOkFunction:main()
    if kf:testIfFireWasTriggeredAndReleased() then                                  -- test if fire was triggered and released                                                   
        kf:callCallbackFunction(kf.callback_stack[#kf.callback_stack]["ok"])        -- call callback
        kf:changeToParentStateFromTheStack()                                        -- change to parent state from the stack
    end
end

------------------------------------------------------------------------------------------------
--- return to the parent dialog, the parent dialog is on the stack.<br/>
-- @tab                 model                       the model<br/>
-- name:                (string)                    the name of the control<br/>
-- display_name:        (string)                    the display name of the control

--- member variables.
-- @table members
KFCallCancelFunction = setmetatable({},{__index = KFControl})
KFCallCancelFunction.__index = KFCallCancelFunction

-------------------------------------------------------------------------------------
--- constructor.
function KFCallCancelFunction:new(model)
    local self = setmetatable(KFControl:new(model), KFCallCancelFunction)
    return self
end

function KFCallCancelFunction:main()
    if kf:testIfFireWasTriggeredAndReleased() then                                  -- test if fire was triggered and released
        kf:changeToParentStateFromTheStack()                                        -- change to parent state from the stack
    end
end

------------------------------------------------------------------------------------------------
--- call a function.<br/>
-- @tab                 model                       the model<br/>
-- name:                (string)                    the name of the control<br/>
-- display_name:        (string)                    the display name of the control<br/>
-- callbacks            (tab)                       table of callback functions. The intention is to have one for each direction / fire that has a function

--- member variables.
-- @table members
KFCallFunction = setmetatable({},{__index = KFControl})
KFCallFunction.__index = KFCallFunction

-------------------------------------------------------------------------------------
--- constructor.
function KFCallFunction:new(model)
    local self = setmetatable(KFControl:new(model), KFCallFunction)
    return self
end

function KFCallFunction:main()
    if kf:testIfFireWasTriggeredAndReleased() then                                  -- test if fire was triggered and released
        kf:callCallbackFunction(self.model["callbacks"]["fire"])                    -- call callback
    end
end

------------------------------------------------------------------------------------
--- Various utility functions.
--
-- @module kfUtils
------------------------------------------------------------------------------------

kfUtils = {}

-- I/O utils
-- ********************************************************************************************************************************

------------------------------------------------------------------------------------
--- read from file.
-- @param                   path                    the path of the file
-- @param                   mode                    the mode e.g "r","rb"
-- @copyright netzzwerg / stack overflow
function kfUtils:read_file(path,mode)
    local file = io.open(path, mode)                                                -- r read mode and b binary mode

    if not file then 
        return nil 
    end

    local content = file:read "*a"                                                  -- *a or *all reads the whole file

    file:close()

    return content
end

-- Time utils
-- ********************************************************************************************************************************

------------------------------------------------------------------------------------
--- do nothing for a period of time.
-- @number                  seconds_to_sleep        time to sleep in seconds
function kfUtils:sleep(seconds_to_sleep)
    local sec = tonumber(os.clock() + seconds_to_sleep);                            -- calculate seconds

    while (os.clock() < sec) do                                                     -- wait calculated seconds
    end
end

-- Table utils
-- ********************************************************************************************************************************

------------------------------------------------------------------------------------
--- create a deep copy of a table.
-- @tab                     t                       table to clone
-- @return                  (tab)                   the deep copy
-- @copyright MihailJP / GitHub
function kfUtils:cloneTable (t) 
    if type(t) ~= "table" then return t end
    local meta = getmetatable(t)
    local target = {}
    for k, v in pairs(t) do
        if type(v) == "table" then
            target[k] = cloneTable(v)
        else
            target[k] = v
        end
    end
    setmetatable(target, meta)
    return target
end

------------------------------------------------------------------------------------
--- create a shallow copy of a table
-- @tab                     t                       table to clone
-- @return                  (tab)                   the copy of the table
-- @copyright MihailJP / GitHub
function kfUtils:copyTable (t)
    if type(t) ~= "table" then return t end
    local meta = getmetatable(t)
    local target = {}
    for k, v in pairs(t) do target[k] = v end
    setmetatable(target, meta)
    return target
end

------------------------------------------------------------------------------------
--- merge two tables
-- @tab                     t1                      table 1
-- @tab                     t2                      table 2
-- @return                  (tab)                   the merged table
-- @copyright James / Stack overflow

function kfUtils:mergeTable(t1, t2)
    for k, v in pairs(t2) do
        if (type(v) == "table") and (type(t1[k] or false) == "table") then
            merge(t1[k], t2[k])
        else
            t1[k] = v
        end
    end
    return t1
end

-- Collision detection utils
-- ********************************************************************************************************************************

------------------------------------------------------------------------------------
--- detect collsions between two rectangles.
-- @func                object                      the object to test
-- @func                target                      the object to test against
-- @return              (bool)                      [true = collision detected |false = no collison] 
-- @copyright love2d (BoundingBox)
function kfUtils:detectCollision(object,target)
    for i=1,#object.collision_targets do
       local object_x = object.x + object.collision_targets[i]["x"]
       local object_y = object.y + object.collision_targets[i]["y"]
       local object_width = object.collision_targets[i]["width"]
       local object_height = object.collision_targets[i]["height"]

       for j=1,#target.collision_targets do
            local target_x = target.x + target.collision_targets[j]["x"]
            local target_y = target.y + target.collision_targets[j]["y"]
            local target_width = target.collision_targets[j]["width"]
            local target_height = target.collision_targets[j]["height"]
 
            if          object_x < target_x + target_width 
                    and target_x < object_x + object_width 
                    and object_y < target_y + target_height 
                    and target_y < object_y + object_height then
                return true
            end
        end
    end

    return false
end

-- View utils
-- ********************************************************************************************************************************

------------------------------------------------------------------------------------
--- display a list of texts and a menu beneath them.
-- @string                  state_name              the name of the state. This is necessary for the menut to get the state controls
-- @tab                     texts                   the list of texts
-- @number                  start_y_position        the start y position of the texts
-- @number                  row_height              the row height of the texts 
function kfUtils:displayTextAndMenu(state_name,texts,start_y_position,row_height)
    local _offset = 1                                                               -- inititialize y position offset for menu (one empty line between text and menu)

    for i,text in pairs(texts) do                                                   -- loop over credits and display them
        love.graphics.printf(text,CENTERED,start_y_position + i * row_height,kf.vars["kf_width"],"center",ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)  
        _offset = _offset + 1                                                       -- increase menu offset
    end

    -- menu (back)
    kfUtils:displayMenu(state_name,start_y_position + (_offset * row_height))       -- display menu
end

------------------------------------------------------------------------------------
--- render a frame and the texts inside, used for example in dialogs.
-- @number                  x                       the x position of the frame
-- @number                  y                       the y position of the frame
-- @number                  width                   the width of the frame
-- @number                  height                  the height of the frame
-- @tab                     texts                   the texts to print in the frame
-- @return                  (number)                the y_offset to calculate the menu under the texts
function kfUtils:displayFrame(x,y,width,height,texts)
    local _y_offset = y + MENU_ROW_HEIGHT                                           -- set the y positon of the texts one row under the frame border

    love.graphics.setColor(kf.MODAL_DIALOG_BLACK_TRANSPARENCY)
    love.graphics.rectangle("fill",x+1,y+1,width-2,height-2)                        -- fill the background of the frame in black
    love.graphics.setColor(kf.COLOR_WHITE)
    love.graphics.rectangle("line",x,y,width,height)                                -- fill the border frame in white
    love.graphics.setColor(kf.COLOR_GREY) 
    love.graphics.rectangle("line",x+1,y+1,width-2,height-2)                        -- fill the inner background in black 

    love.graphics.setColor(kf.COLOR_WHITE)
    for i,text in pairs(texts) do                                                   -- loop over texts and print them
        love.graphics.printf(text,CENTERED,_y_offset,kf.vars["kf_width"],"center",ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)  
        _y_offset = _y_offset + MENU_ROW_HEIGHT                                     -- increase y offset
    end

    return _y_offset
end

------------------------------------------------------------------------------------
--- display a list of controls in a menu style.
-- @string                  state_name              the name of the state. This is necessary to get the state controls
-- @number                  y_position              the y start position of the menu
function kfUtils:displayMenu(state_name,y_position)
    local _y_position = y_position                                                  -- initialize the y position             

    for i,control in pairs(kf.states[state_name].controls) do                       -- loop of controls of actual state 
        local _isDisplayable = true         

        if control.model["display_rule"] ~= nil then                                -- test if diplay rule exists
            _isDisplayable = assert(loadstring('return '..control.model["display_rule"]..'(...)'))() -- skip control if display_rule is negativ
        end
        
        if _isDisplayable then                                                      -- test if the control should be displayed
            love.graphics.setColor(kf.COLOR_WHITE)                                  -- set color to white

            if state_name == kf.actual_state_name then                              -- test if the menu is rendered in a foreground view
                if i == kf.model.actual_control_number then                         -- yes: test if the actual control is the one that is selected
                    love.graphics.setColor(kf.COLOR_SELECTED)                       -- yes: set color to selected
                end
            end

            _name = control.model["display_name"]

            if control.model.displayAttributes and control.model["options_display_names"] ~= nil then
                love.graphics.printf(_name ,MENU_OPTIONS_START,_y_position,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)
                love.graphics.printf(control.model["filler"] ,MENU_OPTIONS_DOUBLEPOINT - #control.model["filler"] * FILLER_X_LENGTH,_y_position,kf.vars["kf_width"],nill,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)

                local _value = ""

                if control.model["isSystemVariable"] then
                    _value = control.model["options_display_names"][kf[control.model["attribute"]]]
                else
                    _value = control.model["options_display_names"][kf.vars[control.model["attribute"]]]
                end
                love.graphics.printf(_value ,MENU_OPTIONS_VALUE,_y_position,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)
            elseif control.model.displayAttributes  and control.model["options_display_names"] == nil then
                love.graphics.printf(_name ,MENU_OPTIONS_START,_y_position,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)
                love.graphics.printf(control.model["filler"] ,MENU_OPTIONS_DOUBLEPOINT - #control.model["filler"] * FILLER_X_LENGTH,_y_position,kf.vars["kf_width"],nill,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)

                local _text = ""
                if control.model["zero_value"] ~= nil and control.model["zero_value"] == kf.vars[control.model["attribute"]] then
                    _text =  control.model["zero_text"]
                else
                    if control.model["isSystemVariable"] then
                        _text = kf[control.model["attribute"]]
                    else                    
                        _text = kf.vars[control.model["attribute"]]
                    end
                end
                love.graphics.printf(_text,MENU_OPTIONS_VALUE,_y_position,kf.vars["kf_width"],nill,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)
            else
                love.graphics.printf(_name,CENTERED,_y_position,kf.vars["kf_width"],"center",ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)                    
            end
        
            _y_position = _y_position + MENU_ROW_HEIGHT                             -- increase y position
        end
    end
end

------------------------------------------------------------------------------------
--- display an image and a counter.
-- @tab                     underlay_views          a table of views to draw as the uderlaying graphics
-- @param                   image                   (image) the image to render
-- @tab                     numbers                 the number images to render
-- @number                  timer_start             the start value of the timer
-- @number                  duration                when you divide the start value and this value you get the number to display
-- @number                  image_y                 the y position of the image
-- @number                  number_y                the y position of the numbers
function kfUtils:overlayImageAndCounter(underlay_views,image,numbers,timer_start,duration,image_y,number_y)
    for key,view in pairs(underlay_views) do                                        -- loop over underlying views
        view()                                                                      -- render view
    end

    local _go_width,_go_height = image:getDimensions()                              -- get dimensions of image
    local _number = math.floor((timer_start - kf.state.model["timer"]) / duration) + 1 -- calculate number

    love.graphics.draw(image,((kf.vars["kf_width"] - _go_width) / kf.CENTER),image_y) -- draw image  
    local _width,_height = numbers[_number]:getDimensions()                         -- get number dimensions
    love.graphics.draw(numbers[_number],((kf.vars["kf_width"] - _width) / kf.CENTER),number_y) -- draw number
end

-- Model utils
-- ********************************************************************************************************************************

------------------------------------------------------------------------------------
--- constructs a unique key bassed on the values of the actual game type dimension values.
-- @string                  state_name              the name of the state where the controls are defined in. 
function kfUtils:getActualGameTypeDescription(state_name)
    local _key = ""                                                                 -- init empty key

    for key,control in pairs(kf.states[state_name].controls) do                     -- loop over contols of the given state
        if kf.vars[control.model["attribute"]] ~= control.model["zero_value"] then  -- test if the control is active or in zero value mode
            if control.model["isPartOfGameType"] then                               -- yes: test if control is part of the game type
                if control.model["options_display_names"] then                      -- yes: test if there is a display name
                    _key = _key .. control.model["display_name"] .. ":" .. control.model["options_display_names"][kf.vars[control.model["attribute"]]] -- yes: add attribute and dispaly name of value to the key
                else
                    _key = _key .. control.model["display_name"] .. ":" .. kf.vars[control.model["attribute"]] -- no: add attribute and value to the key 
                end

                if key < #kf.states[state_name].controls then                       -- test if this is the last control in the give state
                    _key = _key .. " "                                              -- no: add a blance to the key
                end
            end
        end
    end

    return _key                                                                     -- return key
end

------------------------------------------------------------------------------------
--- scroll a text.
-- @param                   text                    the text of the scroller
function kfUtils:scroll_text(text)
    kf.vars["scroll_text_x"] = kf.vars["scroll_text_x"] - 1                         -- scroll text to the left
    if kf.vars["scroll_text_x"] < -SCROLL_TEXT_CHAR_WIDTH then                      -- test if a complete character is scrolled
        kf.vars["scroll_text_x"] = 0                                                -- yes: set scroll register to 0
        kf.vars["scroll_text_offset"] = kf.vars["scroll_text_offset"] + 1           -- increase offset
        if kf.vars["scroll_text_offset"] > #text - kf.SCROLLER_BORDER_8 then        -- test if offset reached the end of the text
            kf.vars["scroll_text_offset"] = 0                                       -- yes: set offset to 0             
        end
    end
end

------------------------------------------------------------------------------------
--- return either a random number or the player number. Used to get a dying or winning sound
-- the decission is done by testing if it is a single palyer game or a multiplayer game
-- @param                   player                  the player to get the player_number from
-- @param                   highest                 the hightest number in the random choice case
function kfUtils:rnd_or_player_number(player,highest)
    if kf.vars["kf_number_of_players"] == kf.NUMBER_OF_PLAYER_IN_A_SINGLE_PLAYER_GAME then -- test if this it is a single player game
        return math.random(1,highest)                                               -- single player game: random choice among the existing samples
    else
        return player.player_number                                                 -- multi player game: choose snd based on the player number
    end
end

-- String utils
-- ********************************************************************************************************************************

------------------------------------------------------------------------------------
--- enlarge string to a given length by filling it up with a given char.
-- @string                  str                     the string
-- @number                  length                  the target length
-- @string                  char                    the char used to fill up 
-- @number                  direction               [0 = in front of the string | 1 = behind the string ]
function kfUtils:normalizeStringLength(str,length,char,direction)
    local _ret = str                                                                -- initialize return value
    local _str_length = string.len(str)                                             -- get length of string
    
    if  _str_length < length then                                                   -- test if string length is lower than the target length 
        _additional = length - _str_length                                          -- yes: get difference between the string length and the target length
        for i =1,_additional do                                                     -- loop over this difference
            if direction == 0 then                                                  -- test if the direction is "is fron of"
                _ret = char .. _ret                                                 -- yes: put filler char in front of the string
            else
                _ret = _ret .. char                                                 -- no: put filler char at the end of the string
            end
        end
    end

    return _ret                                                                     -- return the new string
end

-- Debug utils
-- ********************************************************************************************************************************

------------------------------------------------------------------------------------
--- dump a table in readable form.
-- @tab                     node                    the table
-- @copyright Alundaio / stack overflow
function kfUtils:print_table(node)
    -- to make output beautiful
    local function tab(amt)
        local str = ""
        for i=1,amt do
            str = str .. "\t"
        end
        return str
    end

    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"

    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end

        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then

                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end

                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""

                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end

                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. tab(depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. tab(depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. tab(depth) .. key .. " = '"..tostring(v).."'"
                end

                if (cur_index == size) then
                    output_str = output_str .. "\n" .. tab(depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. tab(depth-1) .. "}"
                end
            end

            cur_index = cur_index + 1
        end

        if (size == 0) then
            output_str = output_str .. "\n" .. tab(depth-1) .. "}"
        end

        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end
  
    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)
end