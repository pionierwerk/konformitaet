# Summary
KonFormität is a framework with a Finite State Engine (FSE) core for the [LÖVE - Free 2D Game Engine](http://love2d.org) written in [Lua](http://lua.org). 
# Intention
It’s aim is to provide all states of a game and their transitions.  While KonFormität handles everything around the game, the creator could concentrate on the implementation of the game itself. 

# FSE
The core of Konformität, the Finite State Engine implementation consist of states and transitions. The engine handles the flow (movement) from one state to another state. If there is a transition defined between these states, the engine process this transition. Even if it is possible to move from one state to another state without a transition, it is recommended to use a  transition, to make the flow more transparent.

## States
Beside implementing the FSE pattern, Konformität also implements the Model, View, Controller (MVC) pattern. The engine itself host a global model as well as every state could have a model of its own. This model(s) are controlled by a controller. State and controller is a synonym. A state could have a view, but it's not mandatory. If a state does not have a view the view of the last state that defines a view is rendered.

## Transitions
Transitions encapsulate functions to call when moving from one state to another state. 

## Flow 
Beside the controller that is processed in a endless lope until an exit to another state is requested, each state could also defien a init and a finally function. A init function is processed only one time when the state is reached, while the finally function is called when the state is left. 

![konformitaet_flow](konformitaet_flow.png)

## Standard states
### States with views
- Logo (Display a picture)
- Story (Display a text)
- Menu (Display either a number of parameters to change or a transition to another state)
	- High scores viewer (Display parameters to change and the result high score lists of each parameter combination)
	- Credits (Display a text)
	- How to play  (Display a text)
- Game (up to you :-)
- Enter your name (Display a menu of characters)
- High scores (Display a list )

### States without views
- init (initialize the game model)
- game starts (reset the game model to the start values)
- game over (actualize high scores with the result of the game)
- quit (quit the programm)

[TBD: Video]

# MVC
Beside implementing the FSE pattern, Konformität also implements the Model, View, Controller (MVC) pattern. 

## Model
The engine itself host a global model as well as every state could have a model of its own.

## View
A state could have a view, but it's not mandatory. 

If a state does not have a view the view of the last state that defines a view is rendered. 

Nested (untested) modal dialogs are possible as well.

## Controller
The model(s) are controlled by a controller. State and controller is a synonym. 
 
The controllers support keyboard as well as the first controller that is connected.

If you like you could use as much controllers Löve2D could handle. In the game for example 8 or more players are not uncomon.

### List of controllers
- wait for timer (wait a defined number of seconds)
- form (handle 4 direction movement callback functions)
- menu (handle 2 direction movement (up,down) and decreasing / inreasing values or options list entries (left,right) )
- quit controller (quit the program)

### Controls
A controller could include controls. Such an control encapsulate functions for the main directions (up,down,left,right) and the fire button / space bar. 

#### List of controls
- selectNumber (decrease, increase numbers or up,down in a list of texts that represent numbers) 
- changeState (transition to another state)
- callOkFunction (in dialogs)
- callCancelFunction(indialogs)
- callFunction (calls a callback function)

# KFML
the configuration of the FSE as well as the MVC configuration is done wie a domain specific language called KonFormitaet Meta Language (KFML). It's a file in JSON format (coming soon, at the moment Lua tables are used). Adding states and transitions as well as add controls to controllers and so one could be done via KFML.  

# Getting started
- [Install Lua](https://www.lua.org/download.html)
- [Install Love2D](https://love2d.org/)
- Create a KonFormität project using the [KonFormität-installer](https://gitlab.com/pionierwerk/konformitaet-initializer)

# Next steps
- Work in progress [Develop with KonFormität](https://gitlab.com/pionierwerk/konformitaet/wikis/home)
- TBD: API Documentation
- TBD: Deployment Description & [build script](https://gitlab.com/pionierwerk/konformitaet-build-script)

# Games using KonFormität
- Willkommen (to be released soon)
